<?php

namespace App\Listeners;

use App\Events\CallLog;
use App\Services\CallLogsServices;
use Illuminate\Contracts\Queue\ShouldQueue;
class CallLogSyncData implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $queue = 'listeners';
    //public $connection = 'sqs';
    public $delay = 10;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderShoppingCart  $event
     * @return void
     */
    public function handle(CallLog $event)
    {   
        echo 'Gọi event';
        CallLogsServices::sync(1);
    }

    /**
     * Get the name of the listener's queue.
     *
     * @return string
     */
    public function viaQueue()
    {
        CallLogsServices::sync(1);
        echo 'listeners';
    }

}