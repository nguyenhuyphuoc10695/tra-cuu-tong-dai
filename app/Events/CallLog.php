<?php

namespace App\Events;

use App\CallLog as CallLogModel;
use Illuminate\Queue\SerializesModels;

class CallLog
{
    use SerializesModels;

    public $order;

    /**
     * Create a new event instance.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }
}