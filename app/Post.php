<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table   = 'posts';
    public $timestamps = true;
    protected  $fillable = ['name', 'category_id', 'content', 'hotnew', 'status', 'structured_id'];
}
