<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Helpers\CallAPI;
use App\CallLog as CallLogModel;
use Permission, Auth;
use App\Services\CallLogsServices;
use App\Events\CallLog as CallLogEvent;

class CallLogsController extends Controller{

	/*
	 * Trang lịch sử gọi
	 */
	public function history(Request $request){
		Permission::required('call_logs');
		$data = [];
		CallLogsServices::sync(1);
		$data['getItems'] = CallLogModel::orderBy('start_time', 'DESC');
		if( $request->get('keyword') ){
			// Tìm theo SĐT
			$data['getItems'] = $data['getItems']->where('from_number', 'LIKE', '%'.$request->get('keyword').'%' );
		}

		// Từ ngày
		$dateFrom = explode('/', $request->get('date_from', date('d/m/Y')) );
		$dateFrom = "{$dateFrom[2]}-{$dateFrom[1]}-{$dateFrom[0]} 00:00:00"; // = Y-m-d H:i:s
		$data['getItems'] = $data['getItems']->where('start_time', '>=', $dateFrom );

		// Đến ngày
		$dateTo = explode('/', $request->get('date_to', date('d/m/Y')) );
		$dateTo = "{$dateTo[2]}-{$dateTo[1]}-{$dateTo[0]} 23:59:59"; // = Y-m-d H:i:s
		$data['getItems'] = $data['getItems']->where('start_time', '<=', $dateTo );

		if( $request->get('ext') ){
			// Nhánh nghe máy
			if( $request->get('ext') == 'miss' ){
				// Lọc cuộc gọi nhỡ
				$data['getItems'] = $data['getItems']->whereNull('ext');
			}else{
				$data['getItems'] = $data['getItems']->where('ext', $request->get('ext') );
			}
		}
		$data['total']['duration'] = $data['getItems']->sum('bill_duration');
		$data['getItems'] = $data['getItems']->paginate( paginationLimit() );

		/*$getData = CallAPI::connect(
			'GET',
			'call',
			[
				'page'     => 1,
				'pageSize' => 50, //paginationLimit()
			]
		);
		dd($getData);*/

		return view('pages.admin.call-logs.history', $data);
	}

	/*
	 * Lấy file ghi âm
	 */
	public function getRecording(Request $request){
		Permission::required('call_logs');
		$getData = CallAPI::connect(
			'GET',
			'call/'.$request->get('id').'/recording'
		);
		return response()->json($getData);
	}

	/*
	 * Báo cáo cuộc gọi theo ca
	 */
	public function report(Request $request){
		Permission::required('call_logs');
		$data = [];
		CallLogsServices::sync(1);
		$data['getItems'] = CallLogModel::orderBy('ext', 'ASC');
		$data['getItems'] = $data['getItems']->select(
			'*',
			\DB::raw('SUM(bill_duration) as bill_duration'),
			\DB::raw('COUNT(id) as total_record'),
		);
		$shiftTime = explode('-', $request->get('shift_time', '00:00-23:59') );
		$shiftTime = [
			'from' => $shiftTime[0].':00',
			'to'   => $shiftTime[1].':59'
		];
		if( $request->get('date_from') ){
			// Từ ngày
			$dateFrom = explode('/', $request->get('date_from') );
			$dateFrom = "{$dateFrom[2]}-{$dateFrom[1]}-{$dateFrom[0]} {$shiftTime['from']}"; // = Y-m-d H:i:s
			$data['getItems'] = $data['getItems']->where('start_time', '>=', $dateFrom );
		}
		if( $request->get('date_to') ){
			// Từ ngày
			$dateTo = explode('/', $request->get('date_to') );
			$dateTo = "{$dateTo[2]}-{$dateTo[1]}-{$dateTo[0]} {$shiftTime['to']}"; // = Y-m-d H:i:s
			$data['getItems'] = $data['getItems']->where('start_time', '<=', $dateTo );
		}
		$data['getItems'] = $data['getItems']->groupBy('ext');
		//$data['total']['duration'] = $data['getItems']->sum('bill_duration');
		$data['getItems'] = $data['getItems']->paginate( paginationLimit() );
		return view('pages.admin.call-logs.report', $data);
	}
}