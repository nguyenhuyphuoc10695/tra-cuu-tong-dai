<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\PostRequest;
use App\Services\PostService;
use Illuminate\Http\Request;
const IS_ACTIVE = 1;

class PostController extends Controller
{
    protected $postService;
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \Permission::required('post_manager');
        $categories = $this->postService->getCategoryList(IS_ACTIVE);
        $data = [];
        $data['categories'] = $categories->toArray();
        return view('pages.admin.posts.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $response = [
            'errors'      => [],
            'status_code' => 200,
            'data'        => []
        ];
//		dd($request->all());
        try {
            $data = $request->only(['id', 'name', 'content', 'category_id', 'hotnew', 'status']);
            $response['data'] = PostService::store($data);
            return response()->json($response, $response['status_code']);
        } catch (\Exception $e) {
            $response['errors'] = ['system' => $e->getMessage()];
            $response['status_code'] = 500;
            return response()->json($response, $response['status_code']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deletePost = $this->postService->delete($id);
        if ($deletePost) {
            return redirect()->route('admin.post.list')->with(['flash_level' => 'success', 'flash_message' => 'Xóa tài liệu thành công']);
        } else {
            return redirect()->route('admin.post.list')->with(['flash_level' => 'danger', 'flash_message' => 'Xóa tài liệu thất bại']);
        }
    }

    public function search(Request $request) {
        $id = $request->id;
        $keyword = $request->keyword;
        $posts = $this->postService->getPostByCategory($id, $keyword);
        foreach($posts as $post) {
            $post->category_name = getCategoryParentName($post->category_id);
        }
//        dd($posts);
        $returnHTML = view('pages.admin.posts.ajax_post')->with('posts', $posts)->render();
        return response()->json(array('html'=>$returnHTML));
//        return response()->json($posts, 200);
    }

    public function searchHotNews(Request $request) {
        $keyword = $request->keyword;
        $posts = $this->postService->getSearchHotNews($keyword);
        foreach($posts as $post) {
            $post->category_name = getCategoryParentName($post->category_id);
        }
//        dd($posts);
        $returnHTML = view('pages.admin.posts.ajax_post')->with('posts', $posts)->render();
        return response()->json(array('html'=>$returnHTML));
//        return response()->json($posts, 200);
    }
}
