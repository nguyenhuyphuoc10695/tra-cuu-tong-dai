<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\CategoryRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;

const IS_ACTIVE = 1;

class CategoryController extends Controller
{
    protected $categoryService;
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \Permission::required('category_manager');
        $categories = $this->categoryService->getCategories(IS_ACTIVE);
//        dd(getCategoryOptions($categories->toArray()));
        $data = [];
        $data['categories'] = $categories->toArray();
        return view('pages.admin.categories.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $response = [
            'errors'      => [],
            'status_code' => 200,
            'data'        => []
        ];
//		dd($request->all());
        try {
            $data = $request->only(['id', 'name', 'parent_id', 'status']);
            $response['data'] = CategoryService::store($data);
            return response()->json($response, $response['status_code']);
        } catch (\Exception $e) {
            $response['errors'] = ['system' => $e->getMessage()];
            $response['status_code'] = 500;
            return response()->json($response, $response['status_code']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteCategory = $this->categoryService->delete($id);
        if ($deleteCategory) {
            return redirect()->route('admin.category.list')->with(['flash_level' => 'success', 'flash_message' => 'Xóa danh mục thành công']);
        } else {
            return redirect()->route('admin.category.list')->with(['flash_level' => 'danger', 'flash_message' => 'Xóa danh mục thất bại']);
        }
    }

    public function getPostByCategory($slug) {
        \Permission::required('member_search');
        $category_id = $_GET['cat'];
        $category = $this->categoryService->getCategory($category_id);
        $categories = Category::where('parent_id', 0)->where('status', 1)->get();
        $sub_category_id = $category->parent_id == 0 ? $category_id : $category->parent_id;
        $sub_categories = Category::where('id', $sub_category_id)->with('children')->first();
//        dd($sub_categories);
        $data = [];
        $data['categories'] = $categories;
        $data['sub_categories'] = $sub_categories;
        $data['category'] = $category;
        return view('pages.admin.categories.post_list', $data);
    }
}
