<?php
namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use Permission;

class IndexController extends Controller{
	/*
	 * Trang chính
	 */
	public function index(){
		Permission::required('member');
		$data = [];
        $categories = Category::where('parent_id', 0)->where('status', 1)->get();
        $data['categories'] = $categories;
        $data['hot_news'] = Post::select('id', 'name', 'category_id', 'content')->where('status', 1)->where('hotnew', 1)->get();
		return view('pages.admin.index', $data);
	}
}