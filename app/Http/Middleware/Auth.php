<?php
namespace App\Http\Middleware;
use Illuminate\Support\Facades\Cookie;
/*
 * Kiểm tra quyền đăng nhập
 */
use Closure;
use App\User;
class Auth
{
     public function handle($request, Closure $next)
    {
    	if( \Auth::check() ){
	    	return $next($request);
    	}
    	return redirect()->route('user.login')->with('notify', ['error' => __('user/general.please_login')]);
    }
}
