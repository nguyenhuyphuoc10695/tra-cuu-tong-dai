<?php

namespace App\Services;

use App\Category;
use App\Post;
use Illuminate\Support\Facades\DB;

class CategoryService
{
    public static function getCategory($id) {
        return Category::find($id);
    }

    public static function getCategories($status)
    {
        return Category::select('id', 'parent_id', 'name', 'structured_id')->where('status', $status)->get();
    }

    public static function store($data)
    {
        $slug = vnStrFilter($data['name']);
        $slug_search = Category::where('slug', '=', $slug)->first();
        if (empty($data['id'])) {
            $category = New Category();
            $category->name = $data['name'];
            $category->slug = $slug_search ? $slug_search->slug.'-'.rand(10000, 99999) : $slug;
            $category->status = $data['status'];
            $category->parent_id = $data['parent_id'];
            $category->structured_id = 0;
            $category->save();
        } else {
            $category = self::getCategory($data['id']);
            if ($category->name != $data['name']) {
                if ($category->slug == $slug) {
                    $category->slug = $category->slug.'-'.rand(10000, 99999);
                } else {
                    $category->slug = $slug;
                }
            }
            $category->name = $data['name'];
//            $category->slug = ($category->slug == $slug && $data['name'] != $category->name) ? $category->slug.'-'.rand(10000, 99999) : $slug;
            $category->status = $data['status'];
            $category->parent_id = $data['parent_id'];
        }

        DB::beginTransaction();
        try {
            if (!empty($data['parent_id'])) {
                $structured_id = Category::where('id','=',$data['parent_id'])->first()->structured_id;
                $category->structured_id = $structured_id . $category->id . "-";
            } else {
                $category->structured_id = $category->id . "-";
            }

            if (!empty($data['id'])) {
                $posts = Post::where('category_id', $data['id'])->get();
                foreach ($posts as $item) {
                    $item->update(['structured_id' => $category->structured_id]);
                }
            }

            $category->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            // Lỗi hệ thống, ajax request sẽ kết nối lại
            throw $e;
        }
        return $data;
    }

    public function delete($id) {
        $category = self::getCategory($id);
        $cate_id_1 = $category->id.'-';
        $cate_id_2 = '-'.$category->id.'-';
        $posts = Post::where('structured_id', 'LIKE', $cate_id_1."%")
            ->orWhere('structured_id', 'LIKE', "%".$cate_id_2."%")->get();
        if ($category->delete()) {
            foreach ($posts as $post) {
                $post->delete();
            }
            return true;
        } else {
            return false;
        }
    }
}
