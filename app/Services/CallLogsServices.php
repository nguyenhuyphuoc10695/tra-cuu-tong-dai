<?php
namespace App\Services;
use Option;
use App\Services\UserServices;
use App\CallLog;
use App\Helpers\CallAPI;

class CallLogsServices
{
	/*
	 * Đồng bộ lịch sử từ API -> server
	 */
	public static function sync($page = 1){
		if( empty( Request()->get('date_from') ) ){
			return;
		}
		$getData = CallAPI::connect(
			'GET',
			'call',
			[
				'page'     => $page,
				'pageSize' => 50, //paginationLimit()
			]
		);
		$updating = true;
		$callExt = Option::get('call_ext', []);
		foreach($getData->docs as $item){
			if( CallLog::where('id', $item->id)->exists() ){
				// Bản ghi đã tồn tại
				$updating = false;
			}else{
				// Bản ghi chưa tồn tại
				if( isset($item->answerTime) ){
					$item->answerTime = toDateTime($item->answerTime / 1000);
				}
				if( isset($item->endTime) ){
					$item->endTime = toDateTime($item->endTime / 1000);
				}
				CallLog::create([
					'id'                 => $item->id,
					'from_number'        => $item->fromNumber,
					'start_time'         => toDateTime($item->startTime / 1000),
					'answer_time'        => $item->answerTime ?? null,
					'end_time'           => $item->endTime ?? null,
					'bill_duration'      => $item->billDuration,
					'recording_duration' => $item->recordingDuration,
					'ext'                => $item->toUser->ext ?? null,
				]);
			}
			if( isset($item->toUser->ext) ){
				$callExt[ $item->toUser->ext ] = $item->toUser->ext;
			}
		}
		Option::update(['call_ext' => $callExt]);
		if( $updating && count($getData->docs) > 0 ){
			// Tiếp tục lấy data
			self::sync($page + 1);
		}
	}
}
