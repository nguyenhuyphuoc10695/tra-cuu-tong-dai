<?php

namespace App\Services;

use App\Category;
use App\Post;
use Illuminate\Support\Facades\DB;

class PostService
{
    public static function getCategoryList($status)
    {
        return Category::select('id', 'parent_id', 'name', 'structured_id')->where('status', $status)->get();
    }

    public static function getPostByCategory($id, $keyword)
    {
        $getItems = Post::select('id', 'name', 'category_id', 'content');
        $getItems = $getItems->where('status', 1);
        if ($id != null) {
            $cate_id_1 = $id.'-';
            $cate_id_2 = '-'.$id.'-';
            $getItems = $getItems->where(function($query) use ($cate_id_1, $cate_id_2) {
                $query->where('structured_id', 'LIKE', $cate_id_1."%")
                    ->orWhere('structured_id', 'LIKE', "%".$cate_id_2."%");
            });
        }

        // Tìm kiếm
//        if( strlen($keyword) > 0 ) {
//            $getItems = $getItems->where('name', 'LIKE', "%".$keyword."%");
//        }
        $getItems = $getItems->where('name', 'LIKE', "%".$keyword."%");

//        return $getItems->toSql();
        $getItems = $getItems->orderBy('id', 'DESC')->get();
        return $getItems;
    }

    public static function getSearchHotNews($keyword)
    {
        $getItems = Post::select('id', 'name', 'category_id', 'content');
        $getItems = $getItems->where('status', 1);
        $getItems = $getItems->where('hotnew', 1);
        $getItems = $getItems->where('name', 'LIKE', "%".$keyword."%");

//        return $getItems->toSql();
        $getItems = $getItems->orderBy('id', 'DESC')->get();
        return $getItems;
    }

    public static function getPost($id) {
        return Post::find($id);
    }

    public static function getPosts($status)
    {
        return Post::select('id', 'category_id', 'name', 'structured_id')->where('status', $status)->get();
    }

    public static function store($data)
    {


        DB::beginTransaction();
        try {
            $category = Category::find($data['category_id']);
            if (empty($data['id'])) {
                $post = New Post();
            } else {
                $post = self::getPost($data['id']);
            }
            $post->name = $data['name'];
            $post->content = $data['content'];
            $post->category_id = $data['category_id'];
            $post->hotnew = $data['hotnew'];
            $post->status = $data['status'];
            $post->structured_id = $category->structured_id;
            $post->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            // Lỗi hệ thống, ajax request sẽ kết nối lại
            throw $e;
        }
        return $data;
    }

    public function delete($id) {
        $post = self::getPost($id);
        return $post->delete();
    }
}
