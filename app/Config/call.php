<?php

return [
	// Ca làm việc
	'shift_time' => [
		[
            'name' => 'Ca sáng',
            'from' => '07:30',
            'to'   => '12:30'
        ],
        [
            'name' => 'Ca chiều',
            'from' => '12:30',
            'to'   => '17:30'
        ],
        [
            'name' => 'Ca tối',
            'from' => '17:30',
            'to'   => '22:00'
        ],
	],

];