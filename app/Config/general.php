<?php

return [
	// Số lượng/trang
	'pagination_limit' => [
		50   => 50,
		100  => 100,
		500  => 500,
		1000 => 1000
	],

	// Xóa các bản lịch sử thao tác sau số tháng
	'logs_cleanup_months' => 1,

    'active' => [
        0 => 'Chưa kích hoạt',
        1 => 'Đã kích hoạt',
    ],
    'active_color' => [
        0 => '#A3AFBD',
        1 => '#39DA8A',
    ],
    'file_type' => [
        0 => 'Tin thường',
        1 => 'Tin nổi bật',
    ],
    'file_type_color' => [
        0 => '#00CFDD',
        1 => '#ff2829',
    ],

];