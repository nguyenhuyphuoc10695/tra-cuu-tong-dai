<?php
/*
 * Link menu bên trái của trang quản trị
 */

$data_menu = [
//	url('/') => [
//		'icon'             => 'bx-home',
//		'count'            => 0,
//		'count_style'      => 'info',
//		'hidden'           => false,
//		'permission'       => 'member',
//		'load_js_language' => []
//	],
	'index' => [
		'icon'             => 'bxs-dashboard',
		'count'            => 0,
		'count_style'      => 'info',
		'hidden'           => false,
		'permission'       => 'member',
		'load_js_language' => []
	],
	'account' => [
		'icon'        => 'bx-user',
		'count'       => 0,
		'count_style' => 'primary',
		'permission'  => 'member',
		'hidden'      => true,
		'subs'        => [
			'profile' => [
				'count'            => 0,
				'count_style'      => 'warning',
				'hidden'           => false,
				'permission'       => 'member',
				'load_js_language' => []
			],
			'edit-profile' => [
				'count'            => 0,
				'count_style'      => 'warning',
				'hidden'           => false,
				'permission'       => 'member',
				'load_js_language' => []
			],
			'change-password' => [
				'count'            => 0,
				'count_style'      => 'warning',
				'hidden'           => false,
				'permission'       => 'member',
				'load_js_language' => []
			],
			'change-avatar' => [
				'count'            => 0,
				'count_style'      => 'warning',
				'hidden'           => false,
				'permission'       => 'member',
				'load_js_language' => []
			],
			'notifications' => [
				'count'            => 0,
				'count_style'      => 'warning',
				'hidden'           => false,
				'permission'       => 'member',
				'load_js_language' => []
			],
			'logs' => [
				'count'            => 0,
				'count_style'      => 'warning',
				'hidden'           => false,
				'permission'       => 'member',
				'load_js_language' => []
			],
		]
	],
	'users' => [
		'icon'        => 'bx-id-card',
		'count'       => 0,
		'count_style' => 'primary',
		'permission'  => 'user_manager',
		'hidden'      => false,
		'subs'        => [
			'list' => [
				'count'            => 0,
				'count_style'      => 'warning',
				'hidden'           => false,
				'permission'       => 'user_manager',
				'load_js_language' => []
			],
			'notifications' => [
				'count'            => 0,
				'count_style'      => 'warning',
				'hidden'           => false,
				'permission'       => 'user_manager',
				'load_js_language' => []
			],
		]
	],
    'category' => [
        'icon'             => 'bxs-grid',
        'count'            => 0,
        'count_style'      => 'primary',
        'hidden'           => false,
        'permission'       => 'category_manager',
        'subs'        => [
            'list' => [
                'count'            => 0,
                'count_style'      => 'warning',
                'hidden'           => false,
                'permission'       => 'category_manager',
                'load_js_language' => []
            ],
        ]
    ],
    'post' => [
        'icon'             => 'bxs-file',
        'count'            => 0,
        'count_style'      => 'primary',
        'hidden'           => false,
        'permission'       => 'post_manager',
        'load_js_language' => [],
        'subs'        => [
            'list' => [
                'count'            => 0,
                'count_style'      => 'warning',
                'hidden'           => false,
                'permission'       => 'post_manager',
                'load_js_language' => []
            ],
        ]
    ],
	'file-manager' => [
		'icon'             => 'bx-folder-open',
		'count'            => 0,
		'count_style'      => 'info',
		'hidden'           => false,
		'permission'       => 'file_manager',
		'load_js_language' => []
	],
	'call-logs' => [
		'icon'        => 'bx-phone',
		'count'       => 0,
		'count_style' => 'primary',
		'permission'  => 'call_logs',
		'hidden'      => false,
		'subs'        => [
			'history' => [
				'count'            => 0,
				'count_style'      => 'warning',
				'hidden'           => false,
				'permission'       => 'call_logs',
				'load_js_language' => []
			],
			'report' => [
				'count'            => 0,
				'count_style'      => 'warning',
				'hidden'           => false,
				'permission'       => 'call_logs',
				'load_js_language' => []
			],
		]
	],
	'settings' => [
		'icon'        => 'bx-cog',
		'count'       => 0,
		'count_style' => 'primary',
		'permission'  => 'admin',
		'hidden'      => false,
		'subs'        => [
			'general' => [
				'count'            => 0,
				'count_style'      => 'warning',
				'hidden'           => false,
				'permission'       => 'admin',
				'load_js_language' => []
			],
			'theme' => [
				'count'            => 0,
				'count_style'      => 'danger',
				'hidden'           => false,
				'permission'       => 'admin',
				'load_js_language' => []
			],
			'admin' => [
				'count'            => 0,
				'count_style'      => 'danger',
				'hidden'           => false,
				'permission'       => 'admin',
				'load_js_language' => []
			],
			'language' => [
				'count'            => 0,
				'count_style'      => 'danger',
				'hidden'           => false,
				'permission'       => 'admin',
				'load_js_language' => []
			],
			'page-info' => [
				'count'            => 0,
				'count_style'      => 'danger',
				'hidden'           => false,
				'permission'       => 'admin',
				'load_js_language' => []
			],
			'permission' => [
				'count'            => 0,
				'count_style'      => 'danger',
				'hidden'           => false,
				'permission'       => 'admin',
				'load_js_language' => []
			]
		]
	],
	'tools' => [
		'icon'        => 'bx-wrench',
		'count'       => 0,
		'count_style' => 'primary',
		'permission'  => 'admin',
		'hidden'      => false,
		'subs'        => [
			'logs' => [
				'count'            => 0,
				'count_style'      => 'warning',
				'hidden'           => false,
				'permission'       => 'admin',
				'load_js_language' => []
			],
			'css-demo' => [
				'count'            => 0,
				'count_style'      => 'danger',
				'hidden'           => false,
				'permission'       => 'admin',
				'load_js_language' => []
			],
		]
	],
];
return $data_menu;