<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class CallLog extends Model
{
    public $timestamps = false;
    //protected $fillable = ['*'];
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $dates = ['start_time', 'answer_time', 'end_time'];
    protected $guarded = [];

}
