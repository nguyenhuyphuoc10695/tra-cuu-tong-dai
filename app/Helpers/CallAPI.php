<?php
namespace App\Helpers;

class CallAPI{
	public static function connect($method, $url, $data = []){
		$url = env('CALL_API_URL').$url;
		$curl = curl_init();
		switch ($method){
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);
			if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			break;
			case "PUT":
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
			break;
			default:
			if ($data)
				$url = sprintf("%s?%s", $url, http_build_query($data));
		}
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'token: '.env('CALL_API_TOKEN'),
			'Content-Type: application/json',
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);  
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 3);     
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		$result = curl_exec($curl);
		if( !$result ){
			die("Kết nối API thất bại, vui lòng thử lại");
		}
		curl_close($curl);
		
		return json_decode($result);
	}
}