@php
    use App\Services\UserServices;
    use App\Services\UserDataServices;
    use App\Helpers\Form;
    use App\Role as RoleModel;
    use App\Helpers\CallAPI;
@endphp
@extends('layouts.admin')
@section('header')
    @parent
    <script src="/assets/plugins/item-list/scripts.js"></script>
    <script src="/assets/plugins/form-ajax-save/scripts.js"></script>
@endsection

@section('content')
    <main class="row mt-2">
        <section class="col-12">
            <div class="card">
                <div class="align-items-center">
                    <div class="row align-items-center mr-0 ml-0">
                        <div class="col-8">
                            <h4 class="card-header">
                                Lịch sử cuộc gọi
                            </h4>
                        </div>
                        <div class="col-4 text-right">
                            <i class="bx bx-sync link mr-2" onclick="runRefreshList()" style="font-size: 25px"></i>
                        </div>
                    </div>
                    <div class="card-header-filter">
                        <div class="row justify-content-end">
                            <div class="col-md-3 pt-0 pl-1 pr-1 pb-1">
                                {!!
                                    Form::text([
                                        'title'         => '',
                                        'placeholder'   => 'Tìm theo số người gọi',
                                        'name'          => 'keyword',
                                        'value'         => '',
                                        'class'         => 'item-list-filter',
                                        'attr'          => 'onkeyup="runRefreshList()"',
                                        'icon'          => 'bx-search',
                                        'icon_position' => 'left'
                                    ])
                                !!}
                            </div>
                            <div class="col-md-3 pt-0 pl-1 pr-1 pb-1">
                                {!!
                                    Form::datePicker([
                                        'placeholder' => 'Từ ngày',
                                        'name'        => 'date_from',
                                        'value'       => date('d/m/Y'),
                                        'position'    => 'bottom',
                                        'format'      => 'day/month/year',
                                        'config'      => [
                                            'allow'=>[
                                                'hours' => [],
                                                'minutes' => false,
                                                'days' => '1-31',
                                                'months' => '1-12',
                                                'weekDay' => [],
                                                'min' => ['y' => date('Y', strtotime('-2 years')), 'm' => 1, 'd' => 1],
                                                'max' => ['y' => date('Y'), 'm' => date('m'), 'd' => date('d')]
                                            ],
                                            'value' => ['day' => '01', 'month' => date('m'), 'year' => date('Y') ]
                                        ],
                                        'class'         => 'item-list-filter',
                                        'attr'          => '',
                                        'icon'          => '',
                                        'icon_position' => 'left',
                                        'onchange'      => 'runRefreshList()'
                                    ])
                                !!}
                            </div>
                            <div class="col-md-3 pt-0 pl-1 pr-1 pb-1">
                                {!!
                                    Form::datePicker([
                                        'placeholder' => 'Đến ngày',
                                        'name'        => 'date_to',
                                        'value'       => date('d/m/Y'),
                                        'position'    => 'bottom',
                                        'format'      => 'day/month/year',
                                        'config'      => [
                                            'allow'=>[
                                                'hours' => [],
                                                'minutes' => false,
                                                'days' => '1-31',
                                                'months' => '1-12',
                                                'weekDay' => [],
                                                'min' => ['y' => date('Y', strtotime('-2 years')), 'm' => 1, 'd' => 1],
                                                'max' => ['y' => date('Y'), 'm' => date('m'), 'd' => date('d')]
                                            ],
                                            'value' => ['day' => date('d'), 'month' => date('m'), 'year' => date('Y') ]
                                        ],
                                        'class'         => 'item-list-filter',
                                        'attr'          => '',
                                        'icon'          => '',
                                        'icon_position' => 'left',
                                        'onchange'      => 'runRefreshList()'
                                    ])
                                !!}
                            </div>
                            <div class="col-md-3 pt-0 pl-1 pr-1 pb-1">
                                {!!
                                    Form::select2([
                                        'title'         => 'Nhánh nghe',
                                        'placeholder'   => '',
                                        'name'          => 'ext',
                                        'class'         => 'item-list-filter',
                                        'attr'          => 'onchange="runRefreshList()"',
                                        'icon'          => '',
                                        'icon_position' => 'right',
                                        'options' => ['' => 'Tất cả', 'miss' => 'Gọi nhỡ'] + Option::get('call_ext', []),
                                        'selected' => [],
                                        'multiple' => false,
                                        'search'   => false
                                    ])
                                !!}
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card-body card-body-table" id="item-list">
                    <div class="table-responsive table-responsive-v">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th style="width: 60px">
                                    STT
                                </th>
                                <th style="width: 200px">
                                    Số người gọi
                                </th>
                                <th style="width: 200px">
                                    Nhánh nghe
                                </th>
                                <th style="width: 200px">
                                    Thời gian gọi
                                </th>
                                <th>
                                	Thời lượng
                                </th>
                                <th class="text-center" style="width: 150px">
                                    Đánh giá
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 0;
                            @endphp
                            @foreach($getItems as $item)
                                @php
                                    $i++;
                                @endphp
                                <tr class="item-list-row" data-id="{{ $i }}">
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                STT
                                            </div>
                                            <div style="width: 60%">
                                                {{ $i }}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                Số người gọi
                                            </div>
                                            <div style="width: 60%">
                                                {!! $item->from_number !!}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                Nhánh nghe
                                            </div>
                                            <div style="width: 60%">
                                                {!! $item->ext ?? '' !!}
                                                @if( $item->ext )
                                                    <span class="btn btn-sm btn-primary link" onclick="items.loadAudio('{{ $item->id }}', true); items.playAudio(this, '{!! $item->from_number !!}', '{{ $item->id }}')" style="padding: 2px 5px">
                                                        <i class="bx bx-play"></i>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                Thời gian gọi
                                            </div>
                                            <div style="width: 60%">
                                                {!! $item->start_time->format('H:i:s d/m') !!}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                Thời lượng
                                            </div>
                                            <div style="width: 60%">
                                                {!! round($item->bill_duration) !!}s
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                {{ __('users/list.table_thead_action') }}
                                            </div>
                                            <div style="width: 60%" class="text-center">
                                                <div class="btn-group">
                                                    <div>
                                                        <button class="btn btn-sm btn-primary"
                                                                type="button" onclick="items.loadAudio('{{ $item->id }}', false); itemList.showDetail(this, '#modal-item-list-detail', '{!! $item->from_number !!}')">
                                                            Chi tiết
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <textarea class="item-json-data hide">@json($item)</textarea>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        <b>
                                            {{ number_format($total['duration']) }}s
                                            ({{ floor($total['duration'] / 60) }} phút)
                                        </b>
                                    </td>
                                    <td>
                                        
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="pagination-center pt-1">
                        {!! $getItems->links() !!}
                    </div>
                    <div class="row align-items-center px-md-2 py-1">
                        <div style="width: calc(100% - 150px)">
                            {{ __('general.total_record') }}: <b>{{ $getItems->total() }}</b>
                        </div>
                        <div style="width: 150px">
                            <div class="">
                                {!!
                                    Form::select2([
                                        'title'         => __('general.pagination_limit'),
                                        'placeholder'   => '',
                                        'name'          => 'pagination_limit',
                                        'class'         => 'item-list-filter',
                                        'attr'          => 'onchange="runRefreshList()"',
                                        'icon'          => '',
                                        'icon_position' => 'right',
                                        'options'       => array_replace( [Option::get('settings__general')['pagination_limit'] ?? 5 => Option::get('settings__general')['pagination_limit'] ?? 5], config('general.pagination_limit') ),
                                        'selected'      => [paginationLimit()],
                                        'multiple'      => false,
                                        'search'        => false
                                    ])
                                !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.card-->
        </section>
    </main>

    <!-- Thông tin tài khoản -->
    <section class="modal fade text-left" id="modal-item-list-detail">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="bx bx-x"></i>
                    </button>
                </div>
                <div class="modal-body custom-scrollbar">
                    <template>
                    	<div class="row-detail">
                    		<audio id="item-detail-audio" controls>
                    			<source src="${url}" type="audio/ogg">
                    		</audio>
                    	</div>
                    	<div class="row-detail">
                            <div class="mb-2">
                        		{!!
                                    Form::select([
                                        'title'         => '',
                                        'placeholder'   => 'Người phụ trách',
                                        'name'          => 'assign',
                                        'class'         => 'w-100',
                                        'attr'          => '',
                                        'icon'          => '',
                                        'icon_position' => 'right',
                                        'options' => call_user_func(function(){
                                            $out = [
                                                '' => 'Chọn'
                                            ];
                                            foreach(\App\User::where('role_id', 2)->where('status', 1)->get() as $item){
                                                $out[$item->id] = $item->name;
                                            }
                                            return $out;
                                        }),
                                        'selected' => [],
                                        'multiple' => false,
                                        'search'   => false
                                    ])
                                !!}
                            </div>
                            <div class="mb-2">
                                {!!
                                    Form::textarea([
                                        'title'       => '',
                                        'placeholder' => 'Nhận xét',
                                        'name'        => 'note',
                                        'value'       => '',
                                        'rows'        => 4,
                                        'class'       => '',
                                        'attr'        => '',
                                    ])
                                !!}
                            </div>
                            <div class="mb-2">
                                {!!
                                    Form::checkbox([
                                        'name'    => 'test',
                                        'class'   => '',
                                        'attr'    => '',
                                        'data' => [
                                            'key1' => 'Đánh giá 1',
                                            'key2' => 'Đánh giá 2',
                                            'key3' => 'Đánh giá 3',
                                            'key4' => 'Đánh giá 4',
                                        ],
                                        'checked' => [],
                                        'disabled' => []
                                    ])
                                !!}
                            </div>
                    	</div>
                    </template>
                    <section></section>
                </div>
                <div class="modal-footer hide"></div>
            </div>
        </div>
    </section>
    <!-- / Thông tin tài khoản -->



@endsection

@section('footer')
    @parent
@endsection

@section('footer-assets')
    @parent
    <script type="text/javascript">
        runRefreshList();
        function runRefreshList() {
            itemList.reload({
                element: '#item-list',
                formFilter: '.item-list-filter',
                data: {page: 1},
                success: function () {

                }
            });
        }

        itemList.pagination({
            element: '#item-list',
            formFilter: '.item-list-filter',
            data: {}
        });
        itemList.autoReload({
            element: '#item-list',
            formFilter: '.item-list-filter',
            data: {},
            timer: 20,
            success: function () {

            }
        });

        /*
         * Ấn nút cập nhật thông tin tài khoản
         */
        function formAjaxSaveCustom(element) {
            formAjaxSave.send({
                element: element,
                success: function () {
                    $('.modal').modal('hide');
                    runRefreshList();
                    toastr.success('{{ __('general.update_success') }}');
                }
            });
        }

        /*
         * Click xem thông báo của tài khoản
         */
        function showUserNotifications(self) {
            var dataEl = $(self).parents('.item-list-row').find('.item-json-data');
            var data = JSON.parse(dataEl.val()).notifications;
            $('#tab-user-list-notification').html(data);
        }

        /*
         * Ấn nút thêm tài khoản
         */
        const items = {
            /*
             * Play audio
             */
            loadAudio: (id, autoPlay) => {
                $.ajax({
                    url: '{{ route('admin.call-logs.get-recording') }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {id: id},
                    success: (res) => {
                       $('#item-detail-audio>source').attr('src', res.url);
                       $('#item-detail-audio')[0].load();
                       if( autoPlay ){
                           $('#item-detail-audio')[0].play();
                       }
                    },
                    error: () => {
                        setTimeout(function(){
                            items.loadAudio(id);
                        }, 2000);
                    }
                })
            },

        	/*
			 * Play audio
        	 */
        	playAudio: (self, phone, id) => {
        		itemList.showDetail(self, '#modal-item-list-detail', phone);
        		items.loadAudio(id, true);
        	}
        };

        $("#modal-item-list-detail").on('hide.bs.modal', function () {
        	$('#item-detail-audio')[0].pause();
        });
    </script>
@endsection