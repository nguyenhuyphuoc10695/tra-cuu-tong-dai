@php
    use App\Services\UserServices;
    use App\Services\UserDataServices;
    use App\Helpers\Form;
    use App\Role as RoleModel;
    use App\Helpers\CallAPI;
@endphp
@extends('layouts.admin')
@section('header')
    @parent
    <script src="/assets/plugins/item-list/scripts.js"></script>
    <script src="/assets/plugins/form-ajax-save/scripts.js"></script>
@endsection

@section('content')
    <main class="row mt-2">
        <section class="col-12">
            <div class="card">
                <div class="align-items-center">
                    <div class="row align-items-center mr-0 ml-0">
                        <div class="col-8">
                            <h4 class="card-header">
                                Lịch sử cuộc gọi
                            </h4>
                        </div>
                        <div class="col-4 text-right">
                            <i class="bx bx-sync link mr-2" onclick="runRefreshList()" style="font-size: 25px"></i>
                        </div>
                    </div>
                    <div class="card-header-filter">
                        <div class="row justify-content-start">
                            <div class="col-md-3 pt-0 pl-1 pr-1 pb-1">
                                {!!
                                    Form::select2([
                                        'title'         => 'Chọn ca',
                                        'placeholder'   => '',
                                        'name'          => 'shift_time',
                                        'class'         => 'item-list-filter',
                                        'attr'          => 'onchange="runRefreshList()"',
                                        'icon'          => '',
                                        'icon_position' => 'right',
                                        'options' => call_user_func(function(){
                                            $out = [
                                                '00:00-23:59' => 'Tất cả'
                                            ];
                                            foreach(config('call.shift_time') as $item){
                                                $shiftTime = "{$item['from']}-{$item['to']}";
                                                $out[ $shiftTime ] = $item['name'].' ('.$shiftTime.')';
                                            }
                                            return $out;
                                        }),
                                        'selected' => [],
                                        'multiple' => false,
                                        'search'   => false
                                    ])
                                !!}
                            </div>
                            <div class="col-md-3 pt-0 pl-1 pr-1 pb-1">
                                {!!
                                    Form::datePicker([
                                        'placeholder' => 'Từ ngày',
                                        'name'        => 'date_from',
                                        'value'       => date('d/m/Y'),
                                        'position'    => 'bottom',
                                        'format'      => 'day/month/year',
                                        'config'      => [
                                            'allow'=>[
                                                'hours' => [],
                                                'minutes' => false,
                                                'days' => '1-31',
                                                'months' => '1-12',
                                                'weekDay' => [],
                                                'min' => ['y' => date('Y', strtotime('-2 years')), 'm' => 1, 'd' => 1],
                                                'max' => ['y' => date('Y'), 'm' => date('m'), 'd' => date('d')]
                                            ],
                                            'value' => ['day' => '01', 'month' => date('m'), 'year' => date('Y') ]
                                        ],
                                        'class'         => 'item-list-filter',
                                        'attr'          => '',
                                        'icon'          => '',
                                        'icon_position' => 'left',
                                        'onchange'      => 'runRefreshList()'
                                    ])
                                !!}
                            </div>
                            <div class="col-md-3 pt-0 pl-1 pr-1 pb-1">
                                {!!
                                    Form::datePicker([
                                        'placeholder' => 'Đến ngày',
                                        'name'        => 'date_to',
                                        'value'       => date('d/m/Y'),
                                        'position'    => 'bottom',
                                        'format'      => 'day/month/year',
                                        'config'      => [
                                            'allow'=>[
                                                'hours' => [],
                                                'minutes' => false,
                                                'days' => '1-31',
                                                'months' => '1-12',
                                                'weekDay' => [],
                                                'min' => ['y' => date('Y', strtotime('-2 years')), 'm' => 1, 'd' => 1],
                                                'max' => ['y' => date('Y'), 'm' => date('m'), 'd' => date('d')]
                                            ],
                                            'value' => ['day' => date('d'), 'month' => date('m'), 'year' => date('Y') ]
                                        ],
                                        'class'         => 'item-list-filter',
                                        'attr'          => '',
                                        'icon'          => '',
                                        'icon_position' => 'left',
                                        'onchange'      => 'runRefreshList()'
                                    ])
                                !!}
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card-body card-body-table" id="item-list">
                    <div class="table-responsive table-responsive-v">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th style="width: 60px">
                                    STT
                                </th>
                                <th style="width: 200px">
                                    Nhánh máy
                                </th>
                                <th>
                                    SL cuộc gọi
                                </th>
                                <th>
                                    Số giây nghe
                                </th>
                                <th>
                                    Số giây TB
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 0;
                                $total = [
                                    'record'       => 0,
                                    'duration'     => 0,
                                    'duration_avg' => 0
                                ];
                            @endphp
                            @foreach($getItems as $item)
                                @php
                                    $i++;
                                    $total['record']       += $item->total_record;
                                    $total['duration']     += $item->bill_duration;
                                    $total['duration_avg'] += ($item->bill_duration / $item->total_record);
                                @endphp
                                <tr class="item-list-row" data-id="{{ $i }}">
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                STT
                                            </div>
                                            <div style="width: 60%">
                                                {{ $i }}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                Nhánh máy
                                            </div>
                                            <div style="width: 60%">
                                                <b>
                                                    {!! $item->ext ?? '<span style="color: red">Gọi nhỡ</span>' !!}
                                                </b>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                SL cuộc gọi
                                            </div>
                                            <div style="width: 60%">
                                                {!! number_format($item->total_record) !!}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                Số giây nghe
                                            </div>
                                            <div style="width: 60%">
                                                {!! number_format( round($item->bill_duration) ) !!}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                Số giây TB
                                            </div>
                                            <div style="width: 60%">
                                                {!! round($item->bill_duration / $item->total_record) !!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        <b>
                                           {{ number_format($total['record']) }}
                                       </b>
                                    </td>
                                    <td>
                                        <b>
                                            {{ number_format($total['duration']) }}
                                            ({{ floor($total['duration'] / 60) }} phút)
                                        </b>
                                    </td>
                                    <td>
                                        <b>
                                            {{ number_format($total['duration_avg']) }}
                                        </b>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div><!--/.card-->
        </section>
    </main>



@endsection

@section('footer')
    @parent
@endsection

@section('footer-assets')
    @parent
    <script type="text/javascript">
        runRefreshList();
        function runRefreshList() {
            itemList.reload({
                element: '#item-list',
                formFilter: '.item-list-filter',
                data: {page: 1},
                success: function () {

                }
            });
        }

        itemList.pagination({
            element: '#item-list',
            formFilter: '.item-list-filter',
            data: {}
        });
        itemList.autoReload({
            element: '#item-list',
            formFilter: '.item-list-filter',
            data: {},
            timer: 20,
            success: function () {

            }
        });

    </script>
@endsection