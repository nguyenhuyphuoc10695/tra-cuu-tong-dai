@extends('layouts.admin')
@section('header')
	@parent
	<script src="/assets/plugins/item-list/scripts.js"></script>
@endsection

@section('content')
	<main class="mt-2">
        <div class="row">
            <div class="col-md-6 mb-2">
                <div class="position-relative ">
                    <input type="text" name="post_keyword_modal"
                           id="post_keyword_modal" class="form-control"
                           placeholder="Tìm kiếm tài liệu..." value="" onclick="getSearchModal()"/>
                    <div class="form-control-position">
                        <i class="bx bx-search"></i>
                    </div>
                </div>
            </div>
        </div>
		<div class="row">
			<div class="col-md-4">
				<div class="accordion collapse-icon accordion-icon-rotate" id="accordionWrapaHot">
					<div class="card collapse-header">
						<div id="headingHot" class="card-header collapsed heading-hot" data-toggle="collapse" data-target="#accordionHot" aria-expanded="true">
                            <span class="collapse-title">
                                <i class="bx bxs-hot"></i>
                                <span class="align-middle">HOT</span>
                            </span>
						</div>
						<div id="accordionHot" data-parent="#accordionWrapaHot" class="collapse show">
							<div class="card-content">
								<div class="card-body pt-1 pb-1 pl-1 pr-1">
									<div class="item-post" style="padding: 10px; margin-bottom: 10px;">
										<div class="position-relative ">
											<input type="text" name="hot_post_keyword"
												   id="hot_post_keyword" class="form-control"
												   placeholder="Tìm kiếm tài liệu..." value="" onkeyup="getHotPostSearch()"/>
											<div class="form-control-position">
												<i class="bx bx-search"></i>
											</div>
										</div>
									</div>
									<div id="hot-post-append">
										@foreach($hot_news as $item)
											<div class="item-post item-list-row" data-id="{{ $item->id }}">
												<a href="javascript: void(0);"
												   onclick="itemList.showDetail(this, '#modal-item-list-detail', '{{ $item->name }}')">
													<p class="post-title">{!! $item->name !!}</p>
													<p class="post-category">{!! getCategoryParentName($item->category_id) !!}</p>
													<textarea class="item-json-data hide">@json($item)</textarea>
												</a>
											</div>
										@endforeach
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@foreach($categories as $key => $item)
				@if($key < 2)
					<div class="col-md-4">
						<div class="accordion collapse-icon accordion-icon-rotate" id="accordionWrapaInfo{!! $item->id !!}">
							<div class="card collapse-header">
								<div id="heading{!! $item->id !!}" class="card-header collapsed heading-info" data-toggle="collapse" data-target="#accordionInfo{!! $item->id !!}" aria-expanded="true">
									<span class="collapse-title">
										<i class="bx bxs-grid"></i>
										<span class="align-middle">{!! $item->name !!}</span>
									</span>
								</div>
								<div id="accordionInfo{!! $item->id !!}" data-parent="#accordionWrapaInfo{!! $item->id !!}" class="collapse show">
									<div class="card-content">
										<div class="card-body pt-1 pb-1 pl-1 pr-1">
											<div class="item-post" style="padding: 10px; margin-bottom: 10px;">
												<div class="position-relative ">
													<input type="text" name="post_keyword_{!! $item->id !!}"
														   id="post_keyword_{!! $item->id !!}" class="form-control"
														   placeholder="Tìm kiếm tài liệu..." value="" onkeyup="getPostSearch({!! $item->id !!})"/>
													<div class="form-control-position">
														<i class="bx bx-search"></i>
													</div>
												</div>
											</div>
											@php
												$cate_id_1 = $item->id.'-';
        										$cate_id_2 = '-'.$item->id.'-';
                                                $posts = \App\Post::select('id', 'name', 'category_id', 'content')->where('status', 1)
														->where('structured_id', 'LIKE', $cate_id_1."%")
														->orWhere('structured_id', 'LIKE', "%".$cate_id_2."%")->get();
											@endphp
											<div id="post-append-{!! $item->id !!}">
												@foreach($posts as $post)
													<div class="item-post item-list-row" data-id="{{ $post->id }}">
														<a href="javascript: void(0);"
														   onclick="itemList.showDetail(this, '#modal-item-list-detail', '{{ $post->name }}')">
															<p class="post-title">{!! $post->name !!}</p>
															<p class="post-category">{!! getCategoryParentName($post->category_id) !!}</p>
															<textarea class="item-json-data hide">@json($post)</textarea>
														</a>
													</div>
												@endforeach
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif
			@endforeach
		</div>
		<div class="row">
			@foreach($categories as $key => $item)
				@if($key > 1)
					<div class="col-md-4">
						<div class="accordion collapse-icon accordion-icon-rotate" id="accordionWrapaInfo{!! $item->id !!}">
							<div class="card collapse-header">
								<div id="heading{!! $item->id !!}" class="card-header collapsed heading-info" data-toggle="collapse" data-target="#accordionInfo{!! $item->id !!}">
									<span class="collapse-title">
										<i class="bx bxs-grid"></i>
										<span class="align-middle">{!! $item->name !!}</span>
									</span>
								</div>
								<div id="accordionInfo{!! $item->id !!}" data-parent="#accordionWrapaInfo{!! $item->id !!}" class="collapse">
									<div class="card-content">
										<div class="card-body pt-1 pb-1 pl-1 pr-1">
											<div class="item-post" style="padding: 10px; margin-bottom: 10px;">
												<div class="position-relative ">
													<input type="text" name="post_keyword_{!! $item->id !!}"
														   id="post_keyword_{!! $item->id !!}" class="form-control"
														   placeholder="Tìm kiếm tài liệu..." value="" onkeyup="getPostSearch({!! $item->id !!})"/>
													<div class="form-control-position">
														<i class="bx bx-search"></i>
													</div>
												</div>
											</div>
											@php
												$cate_id_1 = $item->id.'-';
        										$cate_id_2 = '-'.$item->id.'-';
                                                $posts = \App\Post::select('id', 'name', 'category_id', 'content')->where('status', 1)
														->where('structured_id', 'LIKE', $cate_id_1."%")
														->orWhere('structured_id', 'LIKE', "%".$cate_id_2."%")->get();
											@endphp
											<div id="post-append-{!! $item->id !!}">
												@foreach($posts as $post)
													<div class="item-post item-list-row" data-id="{{ $post->id }}">
														<a href="javascript: void(0);"
														   onclick="itemList.showDetail(this, '#modal-item-list-detail', '{{ $post->name }}')">
															<p class="post-title">{!! $post->name !!}</p>
															<p class="post-category">{!! getCategoryParentName($post->category_id) !!}</p>
															<textarea class="item-json-data hide">@json($post)</textarea>
														</a>
													</div>
												@endforeach
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif
			@endforeach
		</div>
	</main>

	<!-- Thông tin tài liệu -->
	<section class="modal fade text-left" id="modal-item-list-detail">
		<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
			<div class="modal-content">
				<div class="modal-header" style="position: relative">
					<h4 class="modal-title"></h4>
					<button type="button" class="close btn-outline-dark" onclick="changeModalSize()"
							style="position: absolute; top: 24%; right: 85px;">
						<i class="bx bx-expand"></i>
					</button>
					<button type="button" class="close btn-danger" data-dismiss="modal">
						<i class="bx bx-x"></i>
					</button>
				</div>
				<div class="modal-body custom-scrollbar">
					<template>
						<div class="mb-2">
							${content}
						</div>
					</template>
					<section></section>
				</div>
				<div class="modal-footer hide"></div>
			</div>
		</div>
	</section>
	<!-- / Thông tin tài liệu -->

    <!-- Form tìm kiếm -->
    <section class="modal fade text-left" id="modal-item-list-search">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-md">
            <div class="modal-content">
                <div class="modal-header" style="position: relative">
					<div class="position-relative" style="width: 90%;">
						<input type="text" name="post_keyword"
							   id="post_keyword" class="form-control"
							   placeholder="Tìm kiếm tài liệu..." value="" onkeyup="getSearchCategories()"/>
						<div class="form-control-position">
							<i class="bx bx-search"></i>
						</div>
					</div>
                    <button type="button" class="close btn-danger" data-dismiss="modal">
                        <i class="bx bx-x"></i>
                    </button>
                </div>
                <div class="modal-body custom-scrollbar">
					<div class="mb-2">
						<div id="post-append"></div>
					</div>
                </div>
                <div class="modal-footer hide"></div>
            </div>
        </div>
    </section>
    <!-- / Form tìm kiếm -->
@endsection

@section('footer')
	@parent
@endsection

@section('footer-assets')
	@parent
	<script>
		function getSearchModal() {
			$('#modal-item-list-search').modal('show');
			$('#post_keyword').val('');
			$('#post-append').html('');
		}

		function hideSearchModal() {
			$('#modal-item-list-search').modal('hide');
		}

		function changeModalSize() {
			$('#modal-item-list-detail .modal-dialog').toggleClass('modal-lg');
			$('#modal-item-list-detail .modal-dialog').toggleClass('modal-xl');
		}

		function getSearchCategories() {
			let baseUrl = '{!! url('/') !!}';
			let search_text = $('#post_keyword').val();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': '{!! csrf_token() !!}'
				}
			});
			$.ajax({
				url: baseUrl + '/admin/post/get-post-search',
				type: 'POST',
				cache: false,
				data: {
					id: null,
					keyword: search_text,
				},
				success: function (data) {
					if (search_text.length > 0) {
						$('#post-append').html(data.html);
					} else {
						$('#post-append').html('');
					}
				},
				error: function (error) {
					console.log(error);
				}
			});
		}

		function getPostSearch(id) {
			let baseUrl = '{!! url('/') !!}';
			let search_text = $('#post_keyword_' + id).val();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': '{!! csrf_token() !!}'
				}
			});
			$.ajax({
				url: baseUrl + '/admin/post/get-post-search',
				type: 'POST',
				cache: false,
				data: {
					id: id,
					keyword: search_text,
				},
				success: function (data) {
					$('#post-append-' + id).html(data.html);
				},
				error: function (error) {
					console.log(error);
				}
			});
		}
		function getHotPostSearch() {
			let baseUrl = '{!! url('/') !!}';
			let search_text = $('#hot_post_keyword').val();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': '{!! csrf_token() !!}'
				}
			});
			$.ajax({
				url: baseUrl + '/admin/post/get-hot-post-search',
				type: 'POST',
				cache: false,
				data: {
					keyword: search_text,
				},
				success: function (data) {
					$('#hot-post-append').html(data.html);
				},
				error: function (error) {
					console.log(error);
				}
			});
		}
	</script>
    <style>
		.modal .modal-content .modal-header .modal-title {
			width: 87%;
		}
		.post-category strong {
			font-weight: normal !important;
		}
		/* width */
		::-webkit-scrollbar {
			width: 4px;
		}

		/* Track */
		::-webkit-scrollbar-track {
			background: #f1f1f1;
			border-radius: 2px;
		}

		/* Handle */
		::-webkit-scrollbar-thumb {
			background: #888;
			border-radius: 2px;
		}

		/* Handle on hover */
		::-webkit-scrollbar-thumb:hover {
			background: #555;
		}
		.accordion {
			margin-bottom: 20px;
		}
		.collapse {
			height: 290px;
			overflow-y: scroll;
		}
		.accordion .card .card-header {
			padding: 10px !important;
		}
		.item-post {
			border-bottom: 0.7px solid lightgrey;
			padding: 5px 0px;
		}
		.item-post a {
			color: unset;
		}
		.item-post p {
			margin-bottom: 0;
			line-height: 1.5;
		}
		.post-title {
			font-size: 14px;
			font-weight: bold;
		}
		.post-category {
			font-size: 12px;
		}
        .heading-hot {
            color: #fff !important;
            background-color: #FF5B5C !important;
        }
        .heading-hot.collapsed {
            color: #fff !important;
            background-color: #FF5B5C !important;
        }
        .accordion .card .card-header.heading-hot:hover {
            background-color: #FF5B5C !important;
        }
		.heading-info {
			color: #fff !important;
			background-color: #5a8dee !important;
		}
		.heading-info.collapsed {
			color: #fff !important;
			background-color: #5a8dee !important;
		}
		.accordion .card .card-header.heading-info:hover {
			background-color: #5a8dee !important;
		}
    </style>
@endsection