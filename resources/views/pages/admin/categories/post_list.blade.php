@php
    use App\Helpers\Form;

    // Lấy dữ liệu
    $getItems = \App\Post::select('id', 'name', 'category_id', 'structured_id', 'content', 'hotnew', 'status', 'created_at');
    $getItems = $getItems->where('status', 1);
    $cate_id_1 = $_GET['cat'].'-';
    $cate_id_2 = '-'.$_GET['cat'].'-';
    $getItems = $getItems->where(function($query) use ($cate_id_1, $cate_id_2) {
            $query->where('structured_id', 'LIKE', $cate_id_1."%")
                ->orWhere('structured_id', 'LIKE', "%".$cate_id_2."%");
        });
    // Tìm kiếm
    if( !empty( request()->keyword ) ) {
        $getItems = $getItems->where('name', 'LIKE', "%".request()->keyword."%");
    }

    $getItems = $getItems->orderBy('id', 'DESC')->get();


@endphp
@extends('layouts.admin')
@section('header')
    @parent
    <script src="/assets/plugins/item-list/scripts.js"></script>
@endsection

@section('content')
    <main class="row mt-2">
        <section class="col-12">
            <div class="card">
                <div class="align-items-center">
                    <div class="row align-items-center mr-0 ml-0">
                        <div class="col-md-8">
                            <h4 class="card-header pl-1">
                                Danh sách tài liệu
                            </h4>
                        </div>
                        <div class="col-md-4 pt-1">
                            {!!
                                    Form::text([
                                        'title'         => '',
                                        'placeholder'   => 'Tìm theo kiếm tài liệu',
                                        'name'          => 'keyword',
                                        'value'         => '',
                                        'class'         => 'item-list-filter',
                                        'attr'          => 'onkeyup="runRefreshList()"',
                                        'icon'          => 'bx-search',
                                        'icon_position' => 'left'
                                    ])
                                !!}
                        </div>
                    </div>
                    <div class="card-header-filter">
                        <div class="row justify-content-end">
                            <div class="col-md-12">
                                @foreach($categories as $item)
                                    <a href="{!! route('admin.danh_muc', $item->slug) !!}?cat={!! $item->id !!}" type="button" class="btn btn-sm @if($_GET['cat'] == $item->id || $item->id == $category->parent_id) btn-success @else btn-outline-light @endif mr-1 mb-1">{!! $item->name !!}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-md-1">
                                <div style="text-align: center;">
                                    @if($sub_categories->children->count() > 0)
                                        <span>↳</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-11">
                                @foreach($sub_categories->children as $item)
                                    <a href="{!! route('admin.danh_muc', $sub_categories->slug) !!}?cat={!! $item->id !!}" type="button" class="btn btn-sm @if($_GET['cat'] == $item->id) btn-success @else btn-outline-light @endif mr-1 mb-1">{!! $item->name !!}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card-body card-body-table" id="item-list">
                    <div class="table-responsive table-responsive-v">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th style="width: 60px">
                                    STT
                                </th>
                                <th>
                                    Tên tài liệu
                                </th>
                                <th>
                                    Danh mục
                                </th>
                                <th class="text-center" style="width: 150px">
                                    Thao tác
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($getItems as $key => $item)
                                <tr class="item-list-row" data-id="{{ $item->id }}">
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                STT
                                            </div>
                                            <div style="width: 60%">
                                                {{ $key + 1 }}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                Tên
                                            </div>
                                            <div style="width: 60%">
                                                <strong>{!! $item->name !!}</strong>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                Danh mục cha
                                            </div>
                                            <div style="width: 60%">
                                                {!! getCategoryParentName($item->category_id) !!}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                {{ __('users/list.table_thead_action') }}
                                            </div>
                                            <div style="width: 60%" class="text-center">
                                                <div class="btn-group">
                                                    <div class="">
                                                        <button class="btn btn-sm btn-primary"
                                                                type="button" onclick="itemList.showDetail(this, '#modal-item-list-detail', '{{ $item->name }}')">
                                                            <i class="bx bx-show"></i> Xem
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <textarea class="item-json-data hide">@json($item)</textarea>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!--/.card-->
        </section>
    </main>

    <!-- Thông tin tài liệu -->
    <section class="modal fade text-left" id="modal-item-list-detail">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="position: relative">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close btn-outline-dark" onclick="changeModalSize()"
                            style="position: absolute; top: 24%; right: 85px;">
                    <i class="bx bx-expand"></i>
                    </button>
                    <button type="button" class="close btn-danger" data-dismiss="modal">
                        <i class="bx bx-x"></i>
                    </button>
                </div>
                <div class="modal-body custom-scrollbar">
                    <template>
                        <div class="mb-2">
                            ${content}
                        </div>
                    </template>
                    <section></section>
                </div>
                <div class="modal-footer hide"></div>
            </div>
        </div>
    </section>
    <!-- / Thông tin tài liệu -->
@endsection

@section('footer')
    @parent
@endsection

@section('footer-assets')
    @parent
    <script>
        function changeModalSize() {
            $('#modal-item-list-detail .modal-dialog').toggleClass('modal-lg');
            $('#modal-item-list-detail .modal-dialog').toggleClass('modal-xl');
        }

        function runRefreshList() {
            itemList.reload({
                element: '#item-list',
                formFilter: '.item-list-filter',
                data: {page: 1},
                success: function () {

                }
            });
        }

        itemList.autoReload({
            element: '#item-list',
            formFilter: '.item-list-filter',
            data: {},
            timer: 20,
            success: function () {

            }
        });
    </script>
    <style>
        .btn-success.btn-sm {
            padding: 8px 15px !important;
        }
        .modal .modal-content .modal-header .modal-title {
            width: 90%;
        }
    </style>
@endsection