@php
    use App\Helpers\Form;

    // Lấy dữ liệu
    $getItems = \App\Category::where('parent_id', 0)->with('children');

    // Tìm kiếm
    if( !empty( request()->keyword ) ) {
        $getItems = $getItems->where('name', 'LIKE', "%".request()->keyword."%");
    }

    // Lọc theo trạng thái
    if( strlen( request()->status ) > 0 && request()->status != 2 ){
        $getItems = $getItems->where('status', request()->status);
    }

    $getItems = $getItems->orderBy('id', 'DESC')->paginate( paginationLimit() );


@endphp
@extends('layouts.admin')
@section('header')
    @parent
    <script src="/assets/plugins/item-list/scripts.js"></script>
    <script src="/assets/plugins/form-ajax-save/scripts.js"></script>
@endsection

@section('content')
    <main class="row mt-2">
        <section class="col-12">
            <div class="card">
                <div class="align-items-center">
                    <div class="row align-items-center mr-0 ml-0">
                        <div class="col-12">
                            <h4 class="card-header">
                                Danh sách danh mục tài liệu
                            </h4>
                        </div>
                    </div>
                    <div class="card-header-filter">
                        <div class="row justify-content-end">
                            <div class="col-md-5 pt-0 pl-1 pr-1 pb-1">
                                {!!
                                    Form::text([
                                        'title'         => '',
                                        'placeholder'   => 'Tìm theo tên danh mục',
                                        'name'          => 'keyword',
                                        'value'         => '',
                                        'class'         => 'item-list-filter',
                                        'attr'          => 'onkeyup="runRefreshList()"',
                                        'icon'          => 'bx-search',
                                        'icon_position' => 'left'
                                    ])
                                !!}
                            </div>
                            <div class="col-md-3 pt-0 pl-1 pr-1 pb-1">
                                {!!
                                    Form::select([
                                        'title'         => __('users/list.filter_by_gender'),
                                        'placeholder'   => 'Trạng thái',
                                        'name'          => 'status',
                                        'class'         => 'item-list-filter',
                                        'attr'          => 'onchange="runRefreshList()"',
                                        'icon'          => '',
                                        'icon_position' => 'right',
                                        'options' => [
                                            '2' => 'Chọn trạng thái',
                                            '1' => 'Đã kích hoạt',
                                            '0' => 'Chưa kích hoạt',
                                        ],
                                        'selected'      => ['2'],
                                        'multiple'      => false,
                                        'search'        => false
                                    ])
                                !!}
                            </div>
                            <div class="col-md-4 pt-0 pl-1 pr-1 pb-1 text-right">
                                <button class="btn btn-primary" onclick="categoryList.addCategory.showPoup(this)">
                                    <i class="bx bx-plus"></i>
                                    <span class="d-none d-md-inline">Thêm mới danh mục</span>
                                </button>
                                <i class="bx bx-sync link mr-2" onclick="runRefreshList()" style="font-size: 25px"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card-body card-body-table" id="item-list">
                    @if (Session::has('flash_message'))
                        <div class="alert alert-{{ Session::get('flash_level') }} icons-alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="icofont icofont-close-line-circled"></i>
                            </button>
                            <p><strong>{!! Session::get('flash_message') !!}</strong></p>
                        </div>
                    @endif
                    <div class="table-responsive table-responsive-v">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th style="width: 60px">
                                    ID
                                </th>
                                <th>
                                    Tên danh mục
                                </th>
                                <th>
                                    Danh mục cha
                                </th>
                                <th>
                                   Ngày tạo
                                </th>
                                <th>
                                    Trạng thái
                                </th>
                                <th class="text-center" style="width: 150px">
                                    Thao tác
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($getItems as $item)
                                <tr class="item-list-row" data-id="{{ $item->id }}">
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                ID
                                            </div>
                                            <div style="width: 60%">
                                                {{ $item->id }}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                Tên
                                            </div>
                                            <div style="width: 60%">
                                                <strong>{!! $item->name !!}</strong>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                Danh mục cha
                                            </div>
                                            <div style="width: 60%">
                                                {!! getCategoryParentName($item->parent_id) !!}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                Ngày tạo
                                            </div>
                                            <div style="width: 60%">
                                                {!! $item->created_at->format('d/m/Y H:i:s') !!}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                Trạng thái
                                            </div>
                                            <div style="width: 60%; color: {!! config('general.active_color')[$item->status] !!}">
                                                <strong>{!! config('general.active')[$item->status] !!}</strong>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-sm-row">
                                            <div style="width: 40%">
                                                {{ __('users/list.table_thead_action') }}
                                            </div>
                                            <div style="width: 60%" class="text-center">
                                                <div class="btn-group">
                                                    <div class="dropdown">
                                                        <button class="btn btn-sm btn-primary dropdown-toggle"
                                                                type="button" data-toggle="dropdown">
                                                            {{ __('users/list.btn_action_label') }}
                                                        </button>
                                                        <div class="dropdown-menu">
																<span class="dropdown-item link"
                                                                      onclick="itemList.showDetail(this, '#modal-item-list-detail', '{{ $item->name }}'), getCategorySelected(this, {{ $item->parent_id }})">
																	<i class="bx bx-pencil"></i>
																	Sửa
																</span>
                                                            <a class="dropdown-item"
                                                               onclick="return confirm('Bạn có chắc muốn xóa danh mục và các tài liệu liên quan đến danh muc này?');"
                                                               href="{{ route('admin.category.delete', $item->id) }}">
                                                                <i class="bx bx-trash"></i>
                                                                Xóa
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <textarea class="item-json-data hide">@json($item)</textarea>
                                    </td>
                                </tr>
                                @foreach ( $item->children as $child )
                                    <tr class="item-list-row" data-id="{{ $child->id }}">
                                        <td>
                                            <div class="table-sm-row">
                                                <div style="width: 40%">
                                                    ID
                                                </div>
                                                <div style="width: 60%">
                                                    {{ $child->id }}
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="table-sm-row">
                                                <div style="width: 40%">
                                                    Tên
                                                </div>
                                                <div style="width: 60%">
                                                    {!! '↳ '.$child->name !!}
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="table-sm-row">
                                                <div style="width: 40%">
                                                    Danh mục cha
                                                </div>
                                                <div style="width: 60%">
                                                    {!! getCategoryParentName($child->parent_id) !!}
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="table-sm-row">
                                                <div style="width: 40%">
                                                    Ngày tạo
                                                </div>
                                                <div style="width: 60%">
                                                    {!! $child->created_at->format('d/m/Y H:i:s') !!}
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="table-sm-row">
                                                <div style="width: 40%">
                                                    Trạng thái
                                                </div>
                                                <div style="width: 60%; color: {!! config('general.active_color')[$child->status] !!}">
                                                    <strong>{!! config('general.active')[$child->status] !!}</strong>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="table-sm-row">
                                                <div style="width: 40%">
                                                    {{ __('users/list.table_thead_action') }}
                                                </div>
                                                <div style="width: 60%" class="text-center">
                                                    <div class="btn-group">
                                                        <div class="dropdown">
                                                            <button class="btn btn-sm btn-primary dropdown-toggle"
                                                                    type="button" data-toggle="dropdown">
                                                                {{ __('users/list.btn_action_label') }}
                                                            </button>
                                                            <div class="dropdown-menu">
																<span class="dropdown-item link"
                                                                      onclick="itemList.showDetail(this, '#modal-item-list-detail', '{{ $child->name }}'), getCategorySelected(this, {{ $child->parent_id }})">
																	<i class="bx bx-pencil"></i>
																	Sửa
																</span>
                                                                <a class="dropdown-item"
                                                                   onclick="return confirm('Bạn có chắc muốn xóa danh mục này?');"
                                                                   href="{{ route('admin.category.delete', $child->id) }}">
                                                                    <i class="bx bx-trash"></i>
                                                                    Xóa
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <textarea class="item-json-data hide">@json($child)</textarea>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if( $getItems->count() == 0 )
                        <div class="text-center p-2">
                            {{ __('general.item_list_is_empty') }}
                        </div>
                    @endif
                    <div class="pagination-center pt-1">
                        {!! $getItems->links() !!}
                    </div>
                    <div class="row align-items-center px-md-2 py-1">
                        <div style="width: calc(100% - 150px)">
                            {{ __('general.total_record') }}: <b>{{ $getItems->total() }}</b>
                        </div>
                        <div style="width: 150px">
                            <div class="">
                                {!!
                                    Form::select2([
                                        'title'         => __('general.pagination_limit'),
                                        'placeholder'   => '',
                                        'name'          => 'pagination_limit',
                                        'class'         => 'item-list-filter',
                                        'attr'          => 'onchange="runRefreshList()"',
                                        'icon'          => '',
                                        'icon_position' => 'right',
                                        'options'       => array_replace( [Option::get('settings__general')['pagination_limit'] ?? 5 => Option::get('settings__general')['pagination_limit'] ?? 5], config('general.pagination_limit') ),
                                        'selected'      => [paginationLimit()],
                                        'multiple'      => false,
                                        'search'        => false
                                    ])
                                !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.card-->
        </section>
    </main>

    <!-- Thông tin tài khoản -->
    <section class="modal fade text-left" id="modal-item-list-detail">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="bx bx-x"></i>
                    </button>
                </div>
                <div class="modal-body custom-scrollbar">
                    <template>
                        <form class="custom-scrollbar" id="tab-category-edit"
                              action="{{ route('admin.category.update') }}" method="POST">
                            <input type="hidden" name="id" value="${id}">
                            <div class="mb-2">
                                {!!
                                    Form::text([
                                        'title'         => '',
                                        'placeholder'   => 'Tên danh mục',
                                        'name'          => 'name',
                                        'value'         => '${name}',
                                        'class'         => '',
                                        'attr'          => '',
                                        'icon'          => '',
                                        'icon_position' => 'left'
                                    ])
                                !!}
                            </div>
                            <div class="mb-2">
                                <div class="form-group position-relative input-label ">
                                    <label class="input-label-has-content">Thuộc danh mục cha</label>
                                    <select class="form-control " name="parent_id" onfocusin="inputLabel.onFocus(this)"
                                            onfocusout="inputLabel.outFocus(this)" id="category-parent-selected"
                                            data-minimum-results-for-search="Infinity">

                                        <option value="0">Không thuộc danh mục cha</option>
                                        {!! getCategoryOptions($categories) !!}

                                    </select>

                                </div>
                            </div>
                            <div class="mb-2">
                                {!!
                                    Form::select([
                                        'title'         => '',
                                        'placeholder'   => 'Trạng thái ${status}',
                                        'name'          => 'status',
                                        'class'         => '',
                                        'attr'          => 'data-selected="${status}"',
                                        'icon'          => '',
                                        'icon_position' => 'right',
                                        'options' => [
                                            '1' => 'Kích hoạt',
                                            '0' => 'Không kích hoạt',
                                        ],
                                        'selected' => [],
                                        'multiple' => false
                                    ])
                                !!}
                            </div>
                            <div class="form-notify alert alert-danger hide"></div>
                            <div class="text-center mb-2">
                                <button type="button" class="form-save btn btn-primary"
                                        onclick="formAjaxSaveCustom('#tab-category-edit')">
                                    {{ __('user/general.update_profile_btn_label') }}
                                </button>
                            </div>
                        </form>
                    </template>
                    <section></section>
                </div>
                <div class="modal-footer hide"></div>
            </div>
        </div>
    </section>
    <!-- / Thông tin tài khoản -->

    <!-- Thêm tài khoản mới -->
    <section class="modal fade text-left" id="modal-add-category">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="bx bx-x"></i>
                    </button>
                </div>
                <form class="modal-body custom-scrollbar" action="{{ route('admin.category.add') }}" method="POST">
                    <div class="mb-2">
                        {!!
                            Form::text([
                                'title'         => '',
                                'placeholder'   => 'Tên danh mục',
                                'name'          => 'name',
                                'value'         => '',
                                'class'         => '',
                                'attr'          => '',
                                'icon'          => '',
                                'icon_position' => 'left'
                            ])
                        !!}
                    </div>
                    <div class="mb-2">
                        <div class="form-group position-relative input-label ">
                            <label class="input-label-has-content">Thuộc danh mục cha</label>
                            <select class="form-control " name="parent_id" onfocusin="inputLabel.onFocus(this)"
                                    onfocusout="inputLabel.outFocus(this)" data-minimum-results-for-search="Infinity">

                                <option value="0">Không thuộc danh mục cha</option>

                                {!! getCategoryOptions($categories) !!}

                            </select>

                        </div>
                    </div>
                    <div class="mb-2">
                        {!!
                            Form::select([
                                'title'         => '',
                                'placeholder'   => 'Trạng thái',
                                'name'          => 'status',
                                'class'         => '',
                                'attr'          => '',
                                'icon'          => '',
                                'icon_position' => 'right',
                                'options' => [
                                    '1' => 'Kích hoạt',
                                    '0' => 'Không kích hoạt',
                                ],
                                'selected' => ['1'],
                                'multiple' => false
                            ])
                        !!}
                    </div>

                    <div class="form-notify alert alert-danger hide"></div>
                    <div class="text-center mb-2">
                        <button type="button" class="form-save btn btn-primary">
                            Cập nhật
                        </button>
                    </div>
                </form>
                <script type="text/javascript">
                    formAjaxSave.init({
                        element: '#modal-add-category form',
                        success: function () {
                            $('.modal').modal('hide');
                            runRefreshList();
                        }
                    });
                </script>
            </div>
        </div>
    </section>

@endsection

@section('footer')
    @parent
@endsection

@section('footer-assets')
    @parent
    <script type="text/javascript">
        function runRefreshList() {
            itemList.reload({
                element: '#item-list',
                formFilter: '.item-list-filter',
                data: {page: 1},
                success: function () {

                }
            });
        }

        itemList.pagination({
            element: '#item-list',
            formFilter: '.item-list-filter',
            data: {}
        });
        itemList.autoReload({
            element: '#item-list',
            formFilter: '.item-list-filter',
            data: {},
            timer: 20,
            success: function () {

            }
        });

        /*
         * Ấn nút cập nhật thông tin tài khoản
         */
        function formAjaxSaveCustom(element) {
            formAjaxSave.send({
                element: element,
                success: function () {
                    $('.modal').modal('hide');
                    runRefreshList();
                    toastr.success('{{ __('general.update_success') }}');
                }
            });
        }

        /*
         * Click xem thông báo của tài khoản
         */
        function showUserNotifications(self) {
            var dataEl = $(self).parents('.item-list-row').find('.item-json-data');
            var data = JSON.parse(dataEl.val()).notifications;
            $('#tab-user-list-notification').html(data);
        }

        /*
         * Ấn nút thêm tài khoản
         */
        const categoryList = {
            // Thêm tài khoản mới
            addCategory: {
                showPoup: (self) => {
                    itemList.formInsertData('#modal-add-category', self, 'Thêm danh mục tài liệu');
                    //$('#modal-add-user').modal('show');
                },
                submit: () => {
                    // Thêm thành công
                }
            },
        };

        function getCategorySelected(self, id) {
            $("#category-parent-selected option[value= "+ id +"]").prop("selected", "selected")
        }
    </script>
@endsection