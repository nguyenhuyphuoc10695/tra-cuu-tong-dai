@if($posts->count() > 0)
    @foreach($posts as $post)
        <div class="item-post item-list-row" data-id="{{ $post->id }}">
            <a href="javascript: void(0);"
               onclick="itemList.showDetail(this, '#modal-item-list-detail', '{{ $post->name }}'), hideSearchModal()">
                <p class="post-title">{!! $post->name !!}</p>
                <p class="post-category">{!! getCategoryParentName($post->category_id) !!}</p>
                <textarea class="item-json-data hide">@json($post)</textarea>
            </a>
        </div>
    @endforeach
@endif