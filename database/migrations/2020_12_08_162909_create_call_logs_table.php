<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_logs', function (Blueprint $table) {
            $table->string('id', 80)->unique(); // ID cuộc gọi
            $table->primary('id');
            $table->string('from_number', 100); // Số người gọi
            $table->timestamp('start_time'); // Thời gian gọi
            $table->timestamp('answer_time')->nullable(); // Thời gian trả lời
            $table->timestamp('end_time')->nullable(); // Thời gian kết thúc
            $table->float('bill_duration')->default(0); // Thời lượng cuộc gọi
            $table->float('recording_duration')->default(0); // Thời lượng nghe máy
            $table->integer('ext')->nullable(); // Nhánh người nhận
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_logs');
    }
}
